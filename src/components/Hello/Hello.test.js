import React from 'react';
import rendered from 'react-test-renderer';

import Hello from './index';

test('Test show name', () =>{
    const name = "foo"
    const component = rendered.create(
        <Hello name={name}/>
    );

    let tree = component.toJSON();
    expect(tree.children).toEqual(expect.arrayContaining([name]));
})