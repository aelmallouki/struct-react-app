// Dependencies
import React, { Component } from 'react';

// Components
import Hello from './Hello';
import Link from './Link';
import CheckboxWithLabel from './CheckboxWithLabel';

// Shared
import Header from '../shared/components/layout/Header';
import Content from '../shared/components/layout/Content';
import Footer from '../shared/components/layout/Footer';
import logo from '../shared/images/logo.svg';
import './App.css';

class App extends Component {
  render() {
    const name = "foo"
    return (
      <div className="App">
        <Header />
        <Content>
          <img src={logo} className="App-logo" alt="logo"/>
          <Link page="http://foo.bar"><Hello name={name} /></Link>
          <CheckboxWithLabel labelOn="On" labelOff="Off"/>
        </Content>
        <Footer />
      </div>
    );
  }
}

export default App;
