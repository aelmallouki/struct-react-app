import React from 'react';
import { string } from 'prop-types';

export default class CheckboxWithLabel extends React.Component {
    constructor(props){
        super(props);
        this.state = {isChecked: false};

        this.onChange = this.onChange.bind(this);
    }

    onChange(){
        this.setState({isChecked: !this.state.isChecked});
    }

    static propTypes = {
        labelOn: string.isRequired,
        labelOff: string.isRequired
    }
    
    render() {
        const {labelOn, labelOff} = this.props
        return (
            <label>
                <input 
                    type="checkbox"
                    checked={this.state.isChecked}
                    onChange={this.onChange}
                />
                {this.state.isChecked ? labelOn : labelOff}
            </label>
        );
    }
}